#include "propertykeeper.h"
PropertyKeeper * PropertyKeeper::_i=NULL;
PropertyKeeper::PropertyKeeper(QObject *parent) :
    QObject(parent) ,
    _verticalPieceNumber(3) ,
    _horisontalPieceNumber(3)
{
}

PropertyKeeper *PropertyKeeper::Instance()
{
    if(_i == NULL)
        _i=new PropertyKeeper( );

    return _i;
}

int PropertyKeeper::getVerticalPieceNumber()
{
    return _verticalPieceNumber;
}

void PropertyKeeper::setVerticalPieceNumber(int value)
{
    _verticalPieceNumber=value;
    verticalPieceNumberChanged(_verticalPieceNumber);
}

int PropertyKeeper::getHorisontalPieceNumber()
{
    return _horisontalPieceNumber;
}

void PropertyKeeper::setHorisontalPieceNumber(int value)
{
    _horisontalPieceNumber=value;
    horisontalPieceNumberChanged(_horisontalPieceNumber);
}
