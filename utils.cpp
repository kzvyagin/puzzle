#include "utils.h"
#include <QTextCodec>
#include <QScreen>
#include <QtGui/QGuiApplication>
#include <QQmlContext>
#include "propertykeeper.h"
#include <QDir>
#include <QFile>
#include <QDebug>
#include "httpdownloader.h"
#include <QScopedPointer>
#include <QApplication>
Utils::Utils()
{
}

void Utils::downloadRecources(const QScopedPointer<QApplication> & app)
{
    QString dir=QDir::currentPath();
    if(!dir.endsWith("/")){
        dir+="/";
    }
    QDir dirChecker(dir);

    if(!dirChecker.exists("icons"))
        dirChecker.mkdir("icons");
    if(!dirChecker.exists("images"))
        dirChecker.mkdir("images");

    if( ! Utils::isDatabaseExists() ){
        HttpDownloader hd;


        hd.setTarget("http://greatdiet.weebly.com/uploads/7/7/9/7/7797256/puzzle.sqlite");
        hd.download("puzzle.sqlite");

        while(hd.isFinished()==0){
            app->processEvents();
        }


        QStringList imagesList, targetImagesList;
        imagesList<<"birds.jpg"<<"camel.png"<<"flower.jpg"<<"horse.jpg"<<"lion.jpg"<<"squirrel.jpg"<<"tiger.png";
        targetImagesList<<"http://www.weebly.com/uploads/7/7/9/7/7797256/birds.jpg"
                       <<"http://www.weebly.com/uploads/7/7/9/7/7797256/camel.png"
                      <<"http://www.weebly.com/uploads/7/7/9/7/7797256/flower.jpg"
                     <<"http://www.weebly.com/uploads/7/7/9/7/7797256/horse.jpg"
                    <<"http://www.weebly.com/uploads/7/7/9/7/7797256/lion.jpg"
                   <<"http://www.weebly.com/uploads/7/7/9/7/7797256/squirrel.jpg"
                  <<"http://www.weebly.com/uploads/7/7/9/7/7797256/tiger.png";
        for(int i=0;i<imagesList.size();i++){
            hd.setTarget(targetImagesList.at(i));
            hd.download(imagesList.at(i),"images");

            while(hd.isFinished()==0){
                app->processEvents();
            }


            QString withCapital=imagesList.at(i);
            withCapital[0]=imagesList.at(i)[0].toUpper();
            QString iconPath="icons/icon"+withCapital;
            QPixmap puzzleImage;
            puzzleImage.load("images/"+imagesList.at(i));
            QPixmap  result = puzzleImage.scaledToWidth(400);
            result.save(iconPath);



        }
    }

}


void Utils::registerCommonModules(QtQuick2ApplicationViewer * viewer){

     PropertyKeeper *pk=PropertyKeeper::Instance();
     viewer->rootContext()->setContextProperty( "pk",  pk);
}

void Utils::registerInteraction(QtQuick2ApplicationViewer *viewer)
{
    QmlInteraction *ilm=new QmlInteraction(viewer);
    viewer->rootContext()->setContextProperty( "interaction",  ilm);

}

void Utils::registerPixmapProviders(QtQuick2ApplicationViewer *viewer)
{
    viewer->engine()->addImageProvider( "ImagePrv" , new  ImageProvider()  );
    viewer->engine()->addImageProvider( "IconsPrv" ,    new IconsProvider());
}


void Utils::setCodecs(QString codec)
{
//    {
//            QTextCodec::setCodecForTr( QTextCodec::codecForName(codec.toUtf8().data()) );
//            QTextCodec::setCodecForCStrings( QTextCodec::codecForName(codec.toUtf8().data()) );
//    #ifdef Q_OS_WIN32
//            QTextCodec::setCodecForLocale(QTextCodec::codecForName(codec.toUtf8().data()));
//    #endif

//    }
}



QRect Utils::getScreenGeometry()
{
         return QGuiApplication::primaryScreen()->availableGeometry();
}

bool Utils::isDatabaseExists(){
    QString name="puzzle.sqlite";
       QString dir=QDir::currentPath();
       if(!dir.endsWith("/")){
             dir+="/";
         }
       if(QFile::exists(dir+name)){
           QFile fl(dir+name);
                 fl.open(QIODevice::ReadOnly);
                 qint64 _size=fl.size();
                 fl.close();
                 if(_size>=100){
                     qDebug()<<"Db alredy exists"<<"size="<<_size;
                      return true;
                 }
       }

       return false;
}

void Utils::validateDirectories(){
    QString name="puzzle.sqlite";
       QString dir=QDir::currentPath();
       if(!dir.endsWith("/")){
             dir+="/";
         }
       QDir dirChecker(dir);
      if(!dirChecker.exists("icons"))
         dirChecker.mkdir("icons");
      if(!dirChecker.exists("images"))
          dirChecker.mkdir("images");

}
