#ifndef QMLINTERACTION_H
#define QMLINTERACTION_H

#include <QObject>
#include <QVariantList>
#include <QSize>


class QmlInteraction : public QObject
{
    Q_OBJECT
    int _myValue;
    Q_PROPERTY(int myValue READ getMyValue WRITE setMyValue NOTIFY myValueChanged )

    void copySmallIconToClipboard(QString imagePath, QString iconPath);
public:
    explicit QmlInteraction(QObject *parent = 0);
    Q_INVOKABLE int getTotalNumerOfParts();

    Q_INVOKABLE QVariantList getRowById(int id);
    Q_INVOKABLE QVariantList getIdList();

    Q_INVOKABLE int getScreenWidth();
    Q_INVOKABLE int getScreenHeight();

    Q_INVOKABLE void addImageToClipboard(QString imagePath);


    int getMyValue(){return _myValue;}
    void  setMyValue(int value){_myValue=value; emit myValueChanged(_myValue); }


signals:
    void myValueChanged(int);
    void pleaseLoadNewPixmap(int);
public slots:
    
};

#endif
