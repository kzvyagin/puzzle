#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <QQuickImageProvider>
#include "utils.h"
#include <QDebug>
#include "dbengine/dbpersister.h"
class ImageProvider: public QQuickImageProvider
{
     QPixmap puzzleImage;
     QString currentPixmap;
public:
    ImageProvider( );

    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);

public :
    void setNewPuzzlePixmap(int imageId);

};
#endif // IMAGEPROVIDER_H
