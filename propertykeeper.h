#ifndef PROPERTYKEEPER_H
#define PROPERTYKEEPER_H

#include <QObject>

class PropertyKeeper : public QObject
{
    Q_OBJECT

    static PropertyKeeper *_i;
    explicit PropertyKeeper(QObject *parent = 0);
    PropertyKeeper(const PropertyKeeper &);

    int _verticalPieceNumber;
    Q_PROPERTY(int verticalPieceNumber READ getVerticalPieceNumber WRITE setVerticalPieceNumber NOTIFY verticalPieceNumberChanged)

    int _horisontalPieceNumber;
    Q_PROPERTY(int horisontalPieceNumber READ getHorisontalPieceNumber WRITE setHorisontalPieceNumber NOTIFY horisontalPieceNumberChanged)

public:
    static PropertyKeeper *Instance();
    int getVerticalPieceNumber();
    void setVerticalPieceNumber(int value);

    int getHorisontalPieceNumber();
    void setHorisontalPieceNumber(int value);
signals:
    void verticalPieceNumberChanged(int);
    void horisontalPieceNumberChanged(int);
public slots:
    
};

#endif // PROPERTYKEEPER_H
