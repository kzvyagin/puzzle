#ifndef UTILS_H
#define UTILS_H
#include "qtquick2applicationviewer/qtquick2applicationviewer.h"
#include <QPixmap>
#include "qmlinteraction.h"
#include "imageprovider.h"
#include "iconsprovider.h"
class Utils
{
public:
    Utils();
    static void downloadRecources(const QScopedPointer<QApplication> &app);

    static void  registerCommonModules(QtQuick2ApplicationViewer * viewer);
    static void  registerInteraction(QtQuick2ApplicationViewer * viewer);
    static void  registerPixmapProviders(QtQuick2ApplicationViewer * viewer);

    static void  setCodecs(QString codec);
    static bool  isDatabaseExists();
    static QRect getScreenGeometry() ;
    static void validateDirectories();
};

#endif // UTILS_H
