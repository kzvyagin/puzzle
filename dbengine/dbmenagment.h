#ifndef DBMENAGMENT_H
#define DBMENAGMENT_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>

class DBManager : public QObject
{
    Q_OBJECT
    DBManager(){}
    static DBManager *instanse;
    QSqlDatabase db;
    bool isContainsDbInSharedPlace();

public:
    static DBManager * Instance();
    bool openDB(QString extPath=QString());
    QSqlDatabase getDb();
    QSqlError lastError();  
};
#endif // DBMENAGMENT_H
