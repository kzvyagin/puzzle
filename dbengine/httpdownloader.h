#ifndef HTTPDOWNLOADER_H
#define HTTPDOWNLOADER_H
#include <QObject>
#include <QString>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>


class HttpDownloader : public QObject {
    Q_OBJECT
    int downloadingDone;
    QNetworkAccessManager manager;
    QString target;
    QString nameOnLocalHost;
    QString pathOnLocalHost;
public:
    explicit HttpDownloader();
    ~HttpDownloader();

    void setTarget(const QString& t);
    int isFinished(){ return downloadingDone;}
signals:
    void done();

public slots:
    void download(const QString &newName, const QString &downloadPath=0);
    void downloadFinished(QNetworkReply* data);
    void downloadProgress(qint64 recieved, qint64 total);
};

#endif // HTTPDOWNLOADER_H
