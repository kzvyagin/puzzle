#include "httpdownloader.h"

#include <QCoreApplication>
#include <QUrl>
#include <QNetworkRequest>
#include <QFile>
#include <QDebug>
#include <QDir>
HttpDownloader::HttpDownloader() : QObject(0),downloadingDone(0) {
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadFinished(QNetworkReply*)));
}

HttpDownloader::~HttpDownloader() {

}


void HttpDownloader::setTarget(const QString &t) {
    downloadingDone=0;
    this->target = t;
}

void HttpDownloader::downloadFinished(QNetworkReply *data) {
    QString dir;
    if(pathOnLocalHost.isEmpty())
            dir=QDir::currentPath();
    else
        dir=QDir::currentPath()+"/"+pathOnLocalHost;

    if(!dir.endsWith("/")){
          dir+="/";
      }
    QFile localFile(dir+nameOnLocalHost);
    if (!localFile.open(QIODevice::WriteOnly))
        return;
    const QByteArray sdata = data->readAll();
    localFile.write(sdata);
    //qDebug() << sdata;
    localFile.close();
    qDebug()<<Q_FUNC_INFO<<__LINE__<<"downloading done";
    downloadingDone=1;
    emit done();
}

void HttpDownloader::download(const QString & newName, const QString &downloadPath) {
    nameOnLocalHost=newName;
    pathOnLocalHost=downloadPath;
    QUrl url = QUrl::fromEncoded(this->target.toLocal8Bit());
    QNetworkRequest request(url);
    QObject::connect(manager.get(request), SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));

}

void HttpDownloader::downloadProgress(qint64 recieved, qint64 total) {
    qDebug() << Q_FUNC_INFO<< recieved << total;
}
