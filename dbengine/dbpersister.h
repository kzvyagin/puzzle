#ifndef DBPERSISTER_H
#define DBPERSISTER_H
#include "dbmenagment.h"
#include "datastuctures.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QLinkedList>
#include <QHash>
#include <QLinkedList>
#include <QHash>
#include <QDebug>

struct IconData{
  int id;
  QString name;
  QString path;
  IconData():id(0){}
};
class DBPersister
{
    DBPersister();


public:
   static bool addImage(QString name,QString  imgaPath,QString  iconPath);
   static QVariantList getListOfIdOfIconsPaths();
   static QVariantList getRowById(int id);


};

#endif // DBPERSISTER_H
