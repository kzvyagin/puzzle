#include "dbmenagment.h"
#include <QDir>
#include <QDebug>

DBManager * DBManager::instanse=NULL;


DBManager *DBManager::Instance()
{
    if(instanse==NULL){
        instanse=new DBManager();
    }
    return instanse;
}

bool DBManager::openDB(QString extPath){

    db = QSqlDatabase::addDatabase( "QSQLITE" ) ;
    db.setDatabaseName(QDir::currentPath()+"/puzzle.sqlite");
    return db.open();
}

QSqlDatabase DBManager::getDb(){
    return db;
}

QSqlError DBManager::lastError() {
    return db.lastError();
}


bool DBManager::isContainsDbInSharedPlace()
{   QFile fl(QDir::currentPath()+"puzzle.sqllite");

    return fl.exists();
}

