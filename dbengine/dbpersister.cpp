#include "dbpersister.h"


QVariantList DBPersister::getListOfIdOfIconsPaths()
{
    QString querry="SELECT id from IMAGES where icon_path!='' ";

    QSqlQuery SELECT(DBManager::Instance()->getDb());
    if(!SELECT.exec(querry)){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<SELECT.lastError().text();
        return QVariantList();
    }
    QVariantList result;
    while(SELECT.next()){
        result<<SELECT.value(0);
    }
    return result;
}

QVariantList DBPersister::getRowById(int id)
{
    QString querry("SELECT * from IMAGES where id=%1");
    QSqlQuery SELECT(DBManager::Instance()->getDb());
    //qDebug()<<Q_FUNC_INFO<<querry.arg(id);
    if(!SELECT.exec(querry.arg(id))){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<SELECT.lastError().text();
        return QVariantList();
    }
    QVariantList result;
    if(SELECT.next()){
        int collumnsNumber=SELECT.record().count();
        for(int i=0;i<collumnsNumber;i++)
            result<<SELECT.value(i);
    }
    return result;
}


bool DBPersister::addImage(QString name, QString imgaPath, QString iconPath)
{
    qDebug()<<Q_FUNC_INFO<<__LINE__<<"name="<<name<<"imgaPath="<< imgaPath<<"iconPath="<< iconPath;
    QString querry("INSERT INTO IMAGES( name , image_path ,icon_path ) VALUES ('%1','%2','%3')");
    QSqlQuery INSERT(DBManager::Instance()->getDb());
    if(!INSERT.exec(querry.arg(name,imgaPath,iconPath))){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<INSERT.lastError().text()<<"\n"<<(querry.arg(name,imgaPath,iconPath));
        return false;
    }
    return true;
}
