#ifndef ICONSPROVIDER_H
#define ICONSPROVIDER_H


#include <QQuickImageProvider>
#include "utils.h"
#include <QDebug>
#include "dbengine/dbpersister.h"
class IconsProvider : public QQuickImageProvider
{
public:
    IconsProvider();
    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize);
};

#endif // ICONSPROVIDER_H
