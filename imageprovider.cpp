#include "imageprovider.h"
#include "propertykeeper.h"
ImageProvider::ImageProvider(): QQuickImageProvider(QQuickImageProvider::Pixmap){


}


QPixmap ImageProvider::requestPixmap(const QString &id, QSize *size, const QSize &requestedSize)
{


    qDebug()<<Q_FUNC_INFO<<"id="<<id;//<<"Size="<<*size<<"requestedSize="<<requestedSize;
    QStringList lst=id.split("=",QString::SkipEmptyParts);

    if(lst.size()!=2){
        qDebug()<<Q_FUNC_INFO<<__LINE__<<"NO PICTURE";
        QPixmap px(100,100);
        px.fill(QColor("red"));
        return px;
    }


    if( currentPixmap!=lst[0] ){
        bool status;
        currentPixmap=lst[0];
        QString &fileName=lst[0];
        status=puzzleImage.load(fileName);

        QRect rectSize=Utils::getScreenGeometry();
        qDebug()<<Q_FUNC_INFO<<__LINE__<<status<<rectSize.size();
        #ifdef Q_OS_LINUX
            rectSize.setSize(QSize(400,400));
        #endif
        puzzleImage=puzzleImage.scaled(rectSize.size(),Qt::IgnoreAspectRatio);
    }

    PropertyKeeper *pk=PropertyKeeper::Instance();
    int verticalPiecesNumber=pk->getVerticalPieceNumber();
    int horizontalPiecesNumber=pk->getHorisontalPieceNumber();

    int yPieceSize=puzzleImage.height()/horizontalPiecesNumber;
    int xPieceSize=puzzleImage.width()/verticalPiecesNumber;

    int index=lst[1].toInt();
    if(index==-1){
        return puzzleImage;
    }

    int x=0;
    if(index>=horizontalPiecesNumber){
        if( index % horizontalPiecesNumber == 0 )
            x=index/horizontalPiecesNumber;
        else
            x=(index-index%horizontalPiecesNumber)/horizontalPiecesNumber;
    }

    int y=index%horizontalPiecesNumber;

    qDebug()<<Q_FUNC_INFO<<"id="<<id<<"x="<< x << "y=" << y << (x * xPieceSize) << (y * yPieceSize);
    QPixmap  result = puzzleImage.copy( y * yPieceSize, x * xPieceSize, yPieceSize, xPieceSize);


    return result;


}

void ImageProvider::setNewPuzzlePixmap(int imageId)
{
    QVariantList varList=DBPersister::getRowById(imageId);
    QString pixmapPath=varList[3].toString();
    puzzleImage.load(pixmapPath);
}



