import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "CommonWidgets.js" as Widgets
Rectangle {
   id: myProp
   anchors.fill: parent
//    width: 400
//    height:    400
   color: "#AA0000AA"
   MouseArea{
    anchors.fill: parent

   }
   property int scaledFontSize: 14
   Column{
        id: propCollomn
        anchors.left: myProp.left
        anchors.leftMargin: myProp.width/10
        anchors.right: myProp.right
        anchors.rightMargin: myProp.width/10
        anchors.top:  myProp.top
        anchors.topMargin: myProp.height/10
        spacing: 10
        Label{
            width: propCollomn.width
            text: "Строк в паззле"
            color: "yellow"
            font.pixelSize: myProp.scaledFontSize
        }
        Rectangle{
            width: propCollomn.width
            height: 40
            color: "white"
            radius: 5
            TextInput{
                id: inputArea
                anchors.centerIn:  parent
                text : pk.horisontalPieceNumber
                font.pixelSize: parent.height/2
            }
            MouseArea{    anchors.fill: parent;    onClicked: inputArea.focus=true     }
        }
        Label{
            width: propCollomn.width
            text: "Columns in puzzle"
            color: "yellow"
            font.pixelSize: myProp.scaledFontSize
        }
        Rectangle{
            width: propCollomn.width
            height: 40
            color: "white"
            radius: 5
                TextInput{
                    id: inputArea2
                    anchors.horizontalCenter:   parent.horizontalCenter
                    text : pk.verticalPieceNumber
                    font.pixelSize: parent.height/2
                }
                MouseArea{    anchors.fill: parent;    onClicked: inputArea2.focus=true     }
            }

    }

   Row{
       id: buttonsRow
       anchors.left: propCollomn.left
       anchors.right: propCollomn.right
       anchors.top    : propCollomn.bottom
       anchors.topMargin: myProp.height/10
       anchors.bottom: myProp.bottom
       spacing: myProp.width/elementsCount
       property int elementsCount: 3
       Button{
           id: buttonSave
           width:  buttonsRow.width/buttonsRow.elementsCount
           height: 70
           text: "Save"
           onClicked: {
               pk.verticalPieceNumber   = Number( inputArea2.text )
               pk.horisontalPieceNumber = Number( inputArea.text )
              myProp.destroy()
           }
           style: ButtonStyle {
                        label: Text {
                            text: buttonSave.text
                            color: "black"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                           font.pixelSize: myProp.scaledFontSize
                        }
                    }
       }
       Button{
           id: buttonBack
           width:  buttonsRow.width/buttonsRow.elementsCount
           height: 70
           text: "Назад"
           onClicked: {myProp.destroy()}
           style: ButtonStyle {
                        label: Text {
                            text: buttonBack.text
                            color: "black"
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.pixelSize: myProp.scaledFontSize
                        }
                    }
       }
   }


}
