import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    id: root
    anchors.fill: parent
    Image {
        id: fanImg
        anchors.fill: parent
        source: "image://ImagePrv/"+parent.parent.imagePath+"="+"-1"
        Text {
            id: winText
            color: "#fb08cf"
            text: qsTr("You WIN !!!");
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            anchors.centerIn: parent
            font.pixelSize: parent.height/4
        }
    }


    MouseArea{
        anchors.fill: parent
        onReleased: {}
        onPressed: {}
        onClicked: {}
        onDoubleClicked: {
            root.parent.destroy();
            root.destroy()
        }
    }
    ParticleSystem {
        anchors.fill: parent
        id: syssy
        ParticleGroup {
            name: "fire"
            duration: 2000
            durationVariation: 2000
            to: {"splode":1}
        }
        ParticleGroup {
            name: "splode"
            duration: 400
            to: {"dead":1}
            TrailEmitter {
                group: "works"
                emitRatePerParticle: 100
                lifeSpan: 1000
                maximumEmitted: 1200
                size: 8
                velocity: AngleDirection {angle: 270; angleVariation: 45; magnitude: 20; magnitudeVariation: 20;}
                acceleration: PointDirection {y:100; yVariation: 20}
            }
        }
        ParticleGroup {
            name: "dead"
            duration: 1000
            Affector {
                once: true
                onAffected: worksEmitter.burst(400,x,y)
            }
        }

        Timer {
            interval: 6000
            running: true
            triggeredOnStart: true
            repeat: true
            onTriggered:startingEmitter.pulse(100);
        }
        Emitter {
            id: startingEmitter
            group: "fire"
            width: parent.width
            y: parent.height
            enabled: false
            emitRate: 80
            lifeSpan: 6000
            velocity: PointDirection {y:-100;}
            size: 32
        }

        Emitter {
            id: worksEmitter
            group: "works"
            enabled: false
            emitRate: 100
            lifeSpan: 1600
            maximumEmitted: 6400
            size: 8
            velocity: CumulativeDirection {
                PointDirection {y:-100}
                AngleDirection {angleVariation: 360; magnitudeVariation: 80;}
            }
            acceleration: PointDirection {y:100; yVariation: 20}
        }

        ImageParticle {
            groups: ["works", "fire", "splode"]
            source: "qrc:/icons/icons/particle1.png"
            entryEffect: ImageParticle.Scale
        }
    }


    /////////////////////////////////////////////////////////////////////////
    ParticleSystem {
        id: sytem1
    }
    ImageParticle {
        system: sytem1
        source: "qrc:/icons/icons/glowdot.png"
        color: "white"
        colorVariation: 1.0
        alpha: 0.1
    }

    Component {
        id: emitterComp
        Emitter {
            id: container
            Emitter {
                id: emitMore
                system: sytem1
                emitRate: 128
                lifeSpan: 600
                size: 16
                endSize: 8
                velocity: AngleDirection {angleVariation:360; magnitude: 60}
            }

            property int life: 2600
            property real targetX: 0
            property real targetY: 0
            function go() {
                xAnim.start();
                yAnim.start();
                container.enabled = true
            }
            system: sytem1
            emitRate: 32
            lifeSpan: 600
            size: 24
            endSize: 8
            NumberAnimation on x {
                id: xAnim;
                to: targetX
                duration: life
                running: false
            }
            NumberAnimation on y {
                id: yAnim;
                to: targetY
                duration: life
                running: false
            }
            Timer {
                interval: life
                running: true
                onTriggered: container.destroy();
            }
        }
    }

    function customEmit(x,y) {
        //! [0]
        for (var i=0; i<8; i++) {
            var obj = emitterComp.createObject(root);
            obj.x = x
            obj.y = y
            obj.targetX = Math.random() * 240 - 120 + obj.x
            obj.targetY = Math.random() * 240 - 120 + obj.y
            obj.life = Math.round(Math.random() * 2400) + 200
            obj.emitRate = Math.round(Math.random() * 32) + 32
            obj.go();
        }
        //! [0]
    }

    Timer {
        interval: 1000
        triggeredOnStart: true
        running: true
        repeat: true
        onTriggered: customEmit(Math.random() * root.width, Math.random() * root.height)
    }

}

