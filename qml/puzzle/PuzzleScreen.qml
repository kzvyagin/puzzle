import QtQuick 2.0
import "CommonWidgets.js" as Widgets
Rectangle {
    id: puzzleScreen

    property string imagePath: ""

    color: "#222222"
    anchors.fill: parent


    ListModel {
        property int blackPosition : 0
        id: cuttedPixmap
    }
    Component.onCompleted: {
        fillDataToModel()
        //cuttedPixmap.get(0).iVisible=0
        console.log(isSorted())
    }


    function fillDataToModel(){
        var maxIndex= pk.horisontalPieceNumber*pk.verticalPieceNumber;

        var myList=[];


            for(var index=0; index<maxIndex;index++ ){
                var newNumber=Math.round( Math.random() * (( pk.verticalPieceNumber*pk.horisontalPieceNumber ) -1)   );
                if(myList.length==0){
                    myList.push(newNumber)
                }
                else{
                    var count=0;
                    for( ; count<1000; count++ ){
                        if( myList.indexOf(newNumber) === -1 ){
                             myList.push(newNumber);
                            break;
                        }
                        else{
                            newNumber=Math.round( Math.random() *(( pk.verticalPieceNumber*pk.horisontalPieceNumber ) -1)  );
                        }
                    }

                }
                var l_visible = 1
                if(index==0)
                    l_visible=0;

                cuttedPixmap.append( { iColor: "red",    iNumber:newNumber, iVisible: l_visible } )
            }
          //  console.log("myList.lenght="+myList.length)
           // for(var i=0;i<myList.length;i++){
           //     console.log("myList["+i+"]"+myList[i])
           // }
        }

       function isSorted(){
         var index=cuttedPixmap.count

         var prevValue=index;

         for(index-- ;index>=0; index-- ){

             if(prevValue>cuttedPixmap.get(index).iNumber)
                 prevValue=cuttedPixmap.get(index).iNumber
             else
                 return false;
         }
         return true;

     }

    Component {
        id: widgetdelegate
        Item {
            width: grid.cellWidth;
            height: grid.cellHeight

            Rectangle {
                id: im
                state: "inactive"
                //anchors.centerIn: parent
                width: grid.cellWidth - 10;
                height: grid.cellHeight - 10

                 color: iColor
                 visible: iVisible;


                Image {
                    id: rrrr
                    anchors.fill: parent
                    source:   "image://ImagePrv/"+imagePath+"="+iNumber
                }
                border.color: "white"
                border.width: 0

                states: [
                    State {
                        name: "inactive";
                        when: (grid.firstIndexDrag == -1) || (grid.firstIndexDrag == index)
                        PropertyChanges { target: im; border.width: 0}
                    }
                ]
            }

            states: [
                State {
                    name: "inDrag"
                    when: index == grid.firstIndexDrag

                    PropertyChanges { target: im; border.width: 5 }
                    PropertyChanges { target: im; parent: container }

                    PropertyChanges { target: im; anchors.centerIn: undefined }
                    PropertyChanges { target: im; x: coords.mouseX - im.width/2 }
                    PropertyChanges { target: im; y: coords.mouseY - im.height/2 }
                }
            ]
        }
    }

    GridView {

        property int firstIndexDrag: -1
        id: grid
        property int rowElementsCount: pk.horisontalPieceNumber
        property int colElementsCount: pk.verticalPieceNumber
        interactive: false // no flickable
        anchors.fill: parent
        cellWidth: parent.width / rowElementsCount;
        cellHeight: parent.height /colElementsCount;

       model: cuttedPixmap
       // model: widgetmodel
        delegate: widgetdelegate

        //
        Item {
            id: container
            anchors.fill: parent
        }

        MouseArea {
            id: coords
            anchors.fill: parent
            onReleased: {

                var newIndex = grid.indexAt(mouseX, mouseY);
                var oldIndex = grid.firstIndexDrag;

                if(newIndex!==cuttedPixmap.blackPosition){
                    grid.firstIndexDrag = -1
                    return;
                }

                if (grid.firstIndexDrag != -1 && newIndex !== -1 && newIndex !== oldIndex){
                    var oldValue= cuttedPixmap.get(oldIndex);
                    var tmpColor=oldValue.iColor
                    var tmpNum=oldValue.iNumber
                    var tmpVisible=oldValue.iVisible
                    cuttedPixmap.set(oldIndex,cuttedPixmap.get(newIndex))
                    cuttedPixmap.set(newIndex,{iColor:tmpColor,iVisible:tmpVisible,iNumber:tmpNum})
                  // widgetmodel.move(oldIndex, newIndex , 1);
                   cuttedPixmap.blackPosition=oldIndex;
                }
                grid.firstIndexDrag = -1

                if(isSorted()){
                    console.log("YOU WIN !!!!!!!!!!!!!!!!!!!!!!!!!!!")
                     var object =  Widgets.FireworksLayout.createObject( puzzleScreen , {    visible: true} )
                }
                else
                    console.log("still no")


                   // var object =  Widgets.FireworksLayout.createObject( puzzleScreen , {    visible: true} )
            }
            onPressed: {

                var index=grid.indexAt(mouseX, mouseY)

                var element=cuttedPixmap.get(index)
               // console.log("element.iColor="+element.iColor+"black position="+cuttedPixmap.blackPosition+"current pos="+index +"iNumber="+element.iNumber);

                if(element.iVisible===false){
                    console.log("Black")
                    return;
                }

                console.log(index,(cuttedPixmap.blackPosition+1 ),grid.rowElementsCount,index %grid.rowElementsCount === 0, (cuttedPixmap.blackPosition+1 )%grid.rowElementsCount === 0);
                if( !( index === cuttedPixmap.blackPosition+1 ||
                       index === cuttedPixmap.blackPosition-1 ||
                       index === cuttedPixmap.blackPosition + grid.rowElementsCount ||
                       index === cuttedPixmap.blackPosition - grid.rowElementsCount
                       )
//                        ||
//                       (  index!==0 &&
//                         (cuttedPixmap.blackPosition+1)!==grid.rowElementsCount*grid.colElementsCount &&
//                         index %grid.rowElementsCount === 0 &&
//                         (cuttedPixmap.blackPosition+1 )%grid.rowElementsCount === 0 )// не даем брать первую со строки ниже в случае положения черной в конце строки


                        )
                    return;

                grid.firstIndexDrag = grid.indexAt(mouseX, mouseY)
            }

         onDoubleClicked: puzzleScreen.destroy()

        }
    }




}
