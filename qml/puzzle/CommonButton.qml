// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
Rectangle{ // NOT USED
    property string textValue: ""
    property int fontSizeInPixels: 14
    property real scaleKooficent: 1

    signal buttonClicked();
    signal buttonPressed();
    signal buttonRealised();

     property int blockAllSignals: 0
     onBlockAllSignalsChanged: scale=1
//bouble View2
    id: commonButton
    width: 86
    height: 42
    color: "#5affffff"
    radius: 10
    scale: scaleKooficent
    border.width :5
    border.color: "#62dbff"
    //View1
//    gradient: Gradient {
//        GradientStop {
//            position: 0
//            color: "#00cfb4"
//        }

//        GradientStop {
//            position: 0.450
//            color: "#ffffff"
//        }

//        GradientStop {
//            position: 0.800
//            color: "#007f6e"
//        }
//    }
//    border.width :0
//    border.color: "#CC000000"



    Text {
        id: myText
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.right:  parent.right
        anchors.rightMargin: 5
        anchors.centerIn: parent

        text: parent.textValue
        horizontalAlignment: Text.AlignHCenter
        font.underline: false
        font.italic: true
        font.bold: true
        font.pixelSize: fontSizeInPixels*interaction.getXScaleCoefficent()
    }

MouseArea{
    anchors.fill: parent
//    hoverEnabled: true
//    onEntered: {if(blockAllSignals)     return; parent.scale=1.1*scaleKooficent}
//    onExited:  {if(blockAllSignals)     return; parent.scale=1*scaleKooficent  }
    onClicked: {
        if(blockAllSignals)
            return
        buttonClicked();
        //textValue=myText.text

    }
    onPressed: {
        if(blockAllSignals)
            return
        buttonPressed();
    }
    onReleased: {
        if(blockAllSignals)
            return
        buttonRealised();
    }
}
    /*Text {
        id: commonText
        x: 0
        y: 12
        width: 86
        height: 19
        text: ""
        horizontalAlignment: Text.AlignHCenter
        font.underline: false
        font.italic: true
        font.bold: true
        font.pixelSize: 14
    }*/

}


/*
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#004d0e"
        }

        GradientStop {
            position: 0.470
            color: "#cdf39a"
        }

        GradientStop {
            position: 0.750
            color: "#004d0e"
        }
    }

*/
