import QtQuick 2.0
import "colibri"
import "CommonWidgets.js" as Widgets
Rectangle {
    id: desctopScreen
    width: 800
    height: 424
    gradient: Gradient {
        GradientStop { position: 0; color: "#3f3b3c" }
        GradientStop { position: 1; color: "#010103" }
    }

    ListModel {
        id: appModel
    }


    function riseUpScreen(){
        desctopScreen.z=0;
    }

    function createGameField(id){
        console.log(id)
        var object =  Widgets.PuzzleView.createObject( desctopScreen , {    visible: true} )
        object.imagePath=interaction.getRowById(id)[3]
        object.z=1
        desctopScreen.z=-1
        console.log("object.imagePath="+object.imagePath);
    }
    Component.onCompleted: {
            fillModel()
        covers.itemSelected.connect(createGameField)

    }

    function fillModel(){
        appModel.clear()
        var idList=interaction.getIdList();
        console.log("count=" +idList.length )
        for(var i=0;i<idList.length;i++){
            var rowList=interaction.getRowById(idList[i])
            console.log(rowList)
            console.log("image://IconsPrv/"+rowList[4] )
            //appModel.append({ cover: "image://IconsPrv/"+rowList[4] })
              appModel.append({ _id:rowList[0] ,_name: rowList[2] ,  _image:rowList[3], cover: "image://IconsPrv/"+rowList[4] })
        }

        console.log(appModel.count);
    }


    Text {
        id: artistName
        text: "Choose picture for puzzling."
        anchors.horizontalCenter: parent.horizontalCenter
        height:  45
        font.pixelSize: 40
        font.bold: true
        color: "lightgray"
    }

    CLCarousel {
        id: covers
        anchors.top:    artistName.bottom
        anchors.left:   parent.left
        anchors.right:  buttonsArea.left
        anchors.bottom: parent.left
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        coverList: appModel

    }


    function showFileDialog(){
        var fd=Widgets.MyFileDialog.createObject(desctopScreen   ); //{"x": 0, "y": 0}
    }

    function showAppPropDialog(){
        var fd=Widgets.AppProp.createObject(desctopScreen );
    }


    CLStyle {
        id : buttonsStyle
        gradientDefaultOn:  false
        gradientHoveredOn:false
        gradientPressedOn : false
        gradientSelectedOn:false

        colorWhenDefault:  "#90ff3df0"
        colorWhenPressed:  "#90601695"
        colorWhenHovered:  "#90ff3dff"
        colorWhenSelected: "#90ff3dff"
    }

    Item{
        id: buttonsArea

        width: 100
        anchors{
            right: parent.right
            rightMargin: 5
            top: parent.top
            bottom: parent.bottom
        }
        Column{
             id: myCol
             anchors.centerIn: parent
             spacing: 10
             CLButton {
                style: buttonsStyle
                 width:  buttonsArea.width-5
                 height: buttonsArea.width-5
                 text:  qsTr("Settings");
                 onClicked: showAppPropDialog()
              }
             CLButton {
                style: buttonsStyle
                 width:  buttonsArea.width-5
                 height: buttonsArea.width-5
                 text:  qsTr("Add more");
                 onClicked: showFileDialog()
              }
             CLButton {
                style: buttonsStyle
                 width:  buttonsArea.width-5
                 height: buttonsArea.width-5
                 text:  qsTr("Exit");
                 onClicked: Qt.quit()
              }

        }// Column

    }// Rectangle


}
