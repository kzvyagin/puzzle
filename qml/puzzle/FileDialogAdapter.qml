import QtQuick 2.0
import QtQuick.Dialogs 1.0
Rectangle{
    id: fldrct
    color: "#000000AA"
    anchors.fill: parent
    signal fileChoosed(string filePath)
    signal choosingCanceled()

    FileDialog {

        id: fileDialog
        title: "Please choose a file"
        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ]
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            fldrct.fileChoosed(fileDialog.fileUrls);
            interaction.addImageToClipboard(fileDialog.fileUrls)
            fldrct.destroy();
        }
        onRejected: {
            console.log("Canceled")
            choosingCanceled();
            fldrct.destroy();
        }
        Component.onCompleted: visible = true
    }
}
