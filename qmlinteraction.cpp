#include "qmlinteraction.h"
#include "dbengine/dbpersister.h"
#include <QPixmap>
#include <QDebug>
#include <QGuiApplication>
#include <QScreen>

QmlInteraction::QmlInteraction(QObject *parent) :
    QObject(parent)
{
}



int QmlInteraction::getTotalNumerOfParts()
{
    return 9;
}



QVariantList QmlInteraction::getRowById(int id)
{
    if(id>0)
        return DBPersister::getRowById(id);
    return QVariantList();
}

QVariantList QmlInteraction::getIdList()
{
    return  DBPersister::getListOfIdOfIconsPaths();
}

int QmlInteraction::getScreenWidth()
{
    int width=QGuiApplication::primaryScreen()->availableGeometry().width();
    #ifdef Q_OS_LINUX
        width=320*2 ;
    #endif
    return width;
}

int QmlInteraction::getScreenHeight()
{
    int height=QGuiApplication::primaryScreen()->availableGeometry().height();
    #ifdef Q_OS_LINUX
        height=180*2 ;
    #endif
        return height;
}

void QmlInteraction::addImageToClipboard(QString imagePath)
{
    QString iconPath,name;
    name="NewImag!!!";
    if(imagePath.startsWith("file://") ){
        imagePath=imagePath.mid(QString("file://").size());
    }
    QStringList strList=imagePath.split("/");
    iconPath="icons/icon_"+strList.last();
    DBPersister::addImage(name,imagePath,iconPath);
    copySmallIconToClipboard(imagePath,iconPath);

}


void QmlInteraction::copySmallIconToClipboard(QString imagePath,QString iconPath)
{
    QPixmap puzzleImage;
    puzzleImage.load(imagePath);
    QPixmap  result = puzzleImage.scaledToWidth(100);
    result.save(iconPath);
}
