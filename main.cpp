#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer/qtquick2applicationviewer.h"

#include "utils.h"
#include "dbmenagment.h"
#include "qmlinteraction.h"

#include <QQmlContext>
#include <QUrl>
#include <QApplication>
#include <QMessageBox>

//Q_DECL_EXPORT
int main(int argc, char *argv[])
{
  // QScopedPointer<QGuiApplication> app(new QGuiApplication(argc, argv) );
   QScopedPointer<QApplication> app(new QApplication(argc, argv) );
   Utils::validateDirectories();

   Utils::downloadRecources(app);




   DBManager *mngr=DBManager::Instance();
   if( ! mngr->openDB() ){
        qDebug()<<"Title"<<"NO DB";
        return 0;
   }

    QtQuick2ApplicationViewer viewer;
    Utils::registerCommonModules(&viewer);
    Utils::registerInteraction(&viewer);
    Utils::registerPixmapProviders(&viewer);

    QMessageBox::information( 0,"Hello","Screen size x=" +QString::number(QmlInteraction().getScreenWidth())+" y=" + QString::number(QmlInteraction().getScreenWidth()));

 //   viewer.setSource( QUrl("qrc:qml/puzzle/main.qml") );
 //   viewer.showFullScreen();



     viewer.setSource(QUrl("qrc:/qml/puzzle/main.qml"));
     viewer.showExpanded();


    return app->exec();
}
